@extends('layout.layout')

@section('content')
    <div class="min-h-screen bg-gray-100 py-10">
        <h1 class="text-6xl text-center text-gray-800 font-extrabold mb-8">BLOG</h1>
        <div class="container mx-auto px-4">
            <div class="grid grid-cols-1 md:grid-cols-2 lg:grid-cols-3 gap-8">
                @foreach ($posts as $post)
                    <div class="bg-white rounded-lg overflow-hidden shadow-lg hover:shadow-2xl transition-shadow duration-300">

                        <div class="p-6">
                            <h2 class="text-2xl text-gray-800 font-bold mb-2"> {{ $post->title }} </h2>
                            <p class="text-gray-600 mb-4"> {{ Str::limit($post->contenu, 150) }}</p>
                            <a href="/show/{{ $post->id }}" class="text-indigo-600 hover:text-indigo-900 transition-colors duration-200">Read more</a>
                        </div>
                        <div class="px-6 pt-4 pb-2">
                            @foreach ($post->tags as $tag)
                                <span class="inline-block bg-indigo-100 text-indigo-800 text-xs font-semibold px-3 py-1 rounded-full mr-2 mb-2">{{ $tag->tag }}</span>
                            @endforeach
                        </div>
                        @if (Auth::check() && Auth::user()->admin)
                            <div class="px-6 py-4 border-t ">
                                <a href="/post/delete/{{ $post->id }}" class="text-red-500 hover:text-red-700 transition-colors duration-200">Delete Post</a>
                            </div>
                        @endif
                        <div class="px-6 py-4 border-t text-sm text-gray-600">
                            <div class="flex justify-between items-center">
                                <span>By {{ $post->user->name }}</span>
                                <span>{{ $post->created_at->format('M d, Y') }}</span>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </div>
@endsection
