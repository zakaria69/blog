<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Blog</title>
    <link rel="stylesheet" href="https://cdn.tailwindcss.com">
</head>
<body class="bg-gray-200 flex flex-col min-h-screen">

            @include('includs.header')


    <div class="flex-1 flex overflow-hidden">

        @include('includs.aside')


        <main class="flex-1 bg-gray-200">
            @yield('content')
        </main>
    </div>

   @include('includs.footer')

    <script src="https://cdn.tailwindcss.com"></script>
</body>
</html>



