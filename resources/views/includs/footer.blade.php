<footer class="bg-gray-800">
    <div class="max-w-7xl mx-auto px-4 sm:px-6 lg:px-8">
        <div class="py-8 flex flex-wrap items-center justify-between">
            <a href="/" class="text-white text-3xl font-bold hover:text-gray-300">
                📝 Blog
            </a>

            <ul class="flex flex-wrap items-center justify-center text-sm text-white">
                <li>
                    <a href="/" class="hover:text-gray-300 mr-4">Home</a>
                </li>
                @auth
                <li>
                    <a href="/dashboard" class="hover:text-gray-300 mr-4">Dashboard</a>
                </li>
                @endauth
            </ul>
        </div>
        <div class="border-t border-gray-700 pt-4 text-sm text-center text-gray-400">
            © 2024 Your Company. All rights reserved.
        </div>
    </div>
</footer>
