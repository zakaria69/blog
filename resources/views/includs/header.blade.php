<nav class="bg-gray-900 text-white shadow-lg">
    <div class="container mx-auto px-6 py-3 flex justify-between items-center">

        <div class="flex justify-between items-center">
            <a href="/" class="text-xl font-semibold hover:text-gray-200">MyBlog</a>
            <button class="text-gray-500 hover:text-white focus:outline-none sm:hidden">
                <svg class="w-6 h-6" fill="none" stroke="currentColor" viewBox="0 0 24 24">
                    <path d="M4 6h16M4 12h16M4 18h16" />
                </svg>
            </button>
        </div>


        <div class="hidden sm:flex sm:items-center">
            @auth
                <div class="flex items-center space-x-4">
                    <span class="font-medium hover:text-gray-200">Welcome, {{ auth()->user()->name }}</span>
                    <a href="/dashboard" class="px-3 py-2 rounded-md hover:bg-gray-700">Dashboard</a>
                    <form method="POST" action="/logout">
                        @csrf
                        <button type="submit" class="bg-red-500 hover:bg-red-600 text-white font-bold py-2 px-4 rounded">
                            Logout
                        </button>
                    </form>
                </div>
            @endauth

            @guest
                <div class="flex items-center space-x-4">
                    <a href="/" class="text-gray-300 hover:bg-gray-700 hover:text-white px-3 py-2 rounded-md">blog</a>
                    <a href="/login" class="bg-blue-500 hover:bg-blue-600 text-white font-bold py-2 px-4 rounded">
                        Login
                    </a>
                </div>
            @endguest
        </div>
    </div>
</nav>

